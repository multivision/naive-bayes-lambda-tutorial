package com.serverless;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.DefaultAWSCredentialsProviderChain;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.transfer.MultipleFileDownload;
import com.amazonaws.services.s3.transfer.TransferManager;

public class ClassifierHandler implements RequestHandler<Map<String, Object>, ApiGatewayResponse> {

	private static final Logger LOG = LogManager.getLogger(ClassifierHandler.class);

	@Override
	public ApiGatewayResponse handleRequest(Map<String, Object> input, Context context) {
		LOG.info("received: {}", input);

		AmazonS3 s3Client = AmazonS3ClientBuilder.standard().withRegion(Regions.EU_WEST_1)
				.withCredentials(DefaultAWSCredentialsProviderChain.getInstance()).build();
		String tempFolder = System.getProperty("java.io.tmpdir");

		String baseBucket  = "cloudtutorial.functionasaservice.iscte.pt";
		String modelDir    = "model";

		TransferManager transferManager = new TransferManager(new DefaultAWSCredentialsProviderChain());
		File dir = new File(tempFolder);

		MultipleFileDownload download =  transferManager.downloadDirectory(baseBucket, modelDir, dir);
		try {
			download.waitForCompletion();
		} catch (AmazonClientException | InterruptedException e) {
			e.printStackTrace();
		}

		transferManager.shutdownNow();

		BayesClassifier classifier = new BayesClassifier(tempFolder + "/" + modelDir);

		Boolean spam = classifier.isSpam("Free Cash Grants are being funded by the\n" + 
				"U.S. Government and Private Foundations each and every day. These Grants are Funded from Your Tax Dollars. The Private Foundations Use the Grants as a Tax Write-Off! This Free Money can be used for any purpose you can imagine. Start a Business, Go to College, Buy a House, Medical Bills, or Even Personal Needs. Learn How to apply for and receive Pell Grants &amp; Scholarships. There is Free Money available for virtually any use including personal! Even learn how to apply for Low Interest and No Interest Government Loans!</P>\n" + 
				"<P STYLE=\"text-align: justify;\"><B>Government Grant Facts:</B><BR>Want to start a Business? Or Expand your existing Business? Our Government is Giving away Over 5 Billion dollars in Business grants and low-interest loans. More than 10,000,000 deserving people are getting Free Money to Build their Dreams! What are you waiting for? Let Uncle Sam Finance Your Business Today.");
		Boolean ham = classifier.isSpam("On most modern keyboards, there is a key next to your left alt key that opens \n" + 
				"the start menu, which you can press with your left thumb without taking your \n" + 
				"fingers off the home keys. Otherwise ctrl esc always works. Menu 4 gives me a \n" + 
				"new command prompt in my work directory. Menu P N runs Netscape 4 (for \n" + 
				"compatibility testing in my day job, ugh). Mozilla and Emacs are almost always \n" + 
				"open, but I quit lesser-used applications as soon as I'm done using them \n" + 
				"(mostly because my laptop doesn't have a lot of memory).");
		Response responseBody = new Response("Predicted one(should be spam)::" + spam + "Predicted second(should be ham)::" + ham, input);
		return ApiGatewayResponse.builder()
				.setStatusCode(200)
				.setObjectBody(responseBody)
				.setHeaders(Collections.singletonMap("X-Powered-By", "AWS Lambda & serverless"))
				.build();
	}

}
