package com.serverless;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

public class BayesClassifier  {

	private Double totalSpam;
	private Double totalHam;
	private HashMap<String, Double> spammicity;
	private HashMap<String, Double> hammicity;

	public BayesClassifier(String modelDirectory) {
		this.totalSpam  = (Double)this.readObj("totalSpam", modelDirectory);
		this.totalHam   = (Double)this.readObj("totalHam", modelDirectory);
		this.spammicity = (HashMap<String, Double>)this.readObj("spammicity", modelDirectory);
		this.hammicity  = (HashMap<String, Double>)this.readObj("hammicity", modelDirectory);
	}

	private Object readObj(String obj, String directory) {
		Object toReturn = null;
		try{
			FileInputStream fis = new FileInputStream(directory + "/" + obj);
			ObjectInputStream ois = new ObjectInputStream(fis);
			toReturn =  ois.readObject();
			ois.close();
			fis.close();
		}catch(IOException ioe) {
			ioe.printStackTrace();
			return null;
		}catch(ClassNotFoundException c){
			System.out.println("Class not found");
			c.printStackTrace();
			return null;
		}
		return toReturn;
	}
	
	public boolean isSpam(String str){
		WordTokenizer tokenizer   = new WordTokenizer(str);
		HashMap<String, Double> p = new HashMap<String,Double>();
		HashSet<String> table     = tokenizer.createTable();

		for (String word : table) {
			Double spam = this.spammicity.containsKey(word) ? spammicity.get(word) : 0.0;
			Double ham  = this.hammicity.containsKey(word) ? hammicity.get(word) : 0.0;

			if (spam != 0.0 && ham != 0.0) {
				p.put(word, spam/(spam + ham));
			}
		}

		Double eta = 0.0;
		Set<String> pWords = p.keySet();

		//Laplace smoothing
		for (String word : pWords) {
			Double wordP = p.get(word);
			eta += (Math.log(1 - wordP) - Math.log(wordP));
		}

		Double pFinal = 1/(1 + Math.pow(Math.E, eta));

		return (pFinal > 0.5);
	}

	public String printModel() {
		StringBuilder modelString = new StringBuilder();
		modelString.append("Total Spam::" + this.totalSpam + "\n");
		modelString.append("Total Ham::"  + this.totalHam + "\n");
		modelString.append("Spammicity::" + Arrays.toString(this.spammicity.entrySet().toArray()) + "\n");
		modelString.append("Hammicity::"  + Arrays.toString(this.hammicity.entrySet().toArray()) + "\n");
		return modelString.toString();
	}

}
