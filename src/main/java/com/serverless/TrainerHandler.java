package com.serverless;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Collections;
import java.util.Map;
import java.util.Scanner;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.DefaultAWSCredentialsProviderChain;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.transfer.MultipleFileUpload;
import com.amazonaws.services.s3.transfer.TransferManager;
import com.amazonaws.services.s3.transfer.TransferManagerBuilder;

public class TrainerHandler implements RequestHandler<Map<String, Object>, ApiGatewayResponse> {

	private static final Logger LOG = LogManager.getLogger(TrainerHandler.class);

	@Override
	public ApiGatewayResponse handleRequest(Map<String, Object> input, Context context) {
		LOG.info("received: {}", input);

		AmazonS3 s3Client = AmazonS3ClientBuilder.standard().withRegion(Regions.EU_WEST_1)
				.withCredentials(DefaultAWSCredentialsProviderChain.getInstance()).build();
		String tempFolder = System.getProperty("java.io.tmpdir");

		String baseBucket  = "cloudtutorial.functionasaservice.iscte.pt";
		String trainingDir = "training";
		String labelsDir   = "labels";
		String labelsFile  = "SPAMTrain.label";

		if (!s3Client.doesObjectExist(baseBucket, labelsDir + "/" + labelsFile)) {
			LOG.info("Can't find " + labelsDir + "/" + labelsFile + " at "  + baseBucket);

			Response responseBody = new Response("Can't find " + labelsDir + "/" + labelsFile + " at "  + baseBucket, input);
			return ApiGatewayResponse.builder()
			.setStatusCode(200)
			.setObjectBody(responseBody)
			.setHeaders(Collections.singletonMap("X-Powered-By", "AWS Lambda & serverless"))
			.build();
		}

		File localLabelsFile       = new File(tempFolder + "/" + labelsFile );

		LOG.info("Processing::" + labelsFile);
		
		try {
			s3Client.getObject(new GetObjectRequest(baseBucket, labelsDir + "/" + labelsFile), localLabelsFile);
		}catch ( Exception e) {
			LOG.info("Can't find " + labelsDir + "/" + labelsFile + " at "  + baseBucket);

			Response responseBody = new Response("Can't find " + labelsDir + "/" + labelsFile + " at "  + baseBucket, input);
			return ApiGatewayResponse.builder()
			.setStatusCode(200)
			.setObjectBody(responseBody)
			.setHeaders(Collections.singletonMap("X-Powered-By", "AWS Lambda & serverless"))
			.build();
		}

		BayesTrainer modelCoach = new BayesTrainer();
		File localMessageFile   = new File(tempFolder + "/message" );

		try {
			Scanner scanner = new Scanner(localLabelsFile);
			while (scanner.hasNextLine()) {
				String[] labeledFile = scanner.nextLine().split(" ");
				if (!s3Client.doesObjectExist(baseBucket, trainingDir + "/" + labeledFile[1])) {
					continue;
				}
				try {
					s3Client.getObject(new GetObjectRequest(baseBucket, trainingDir + "/" + labeledFile[1]), localMessageFile);
				}catch ( Exception e) {
					LOG.info("Failed dowloading " + labelsDir + "/" + labelsFile + " at "  + baseBucket);
				}
				Boolean isHam = labeledFile[0].equalsIgnoreCase("1") ? true : false;
				modelCoach.train(isHam, this.readFileMessage(localMessageFile.getAbsolutePath()));
			}
			scanner.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		modelCoach.printModel();
		modelCoach.saveModel(tempFolder + "/model");

		TransferManager transferManager = new TransferManager(new DefaultAWSCredentialsProviderChain());
		MultipleFileUpload upload = transferManager.uploadDirectory(baseBucket,
				"model", new File(tempFolder + "/model"), false);

		try {
		    upload.waitForCompletion();
		} catch (AmazonServiceException | InterruptedException e) {
			e.printStackTrace();
		}
		transferManager.shutdownNow();

		Response responseBody = new Response("Training complete. Model saved.", input);
		return ApiGatewayResponse.builder()
				.setStatusCode(200)
				.setObjectBody(responseBody)
				.setHeaders(Collections.singletonMap("X-Powered-By", "AWS Lambda & serverless"))
				.build();
	}

	private String readFileMessage(String fileLocation) {
		File file = new File(fileLocation);
		FileInputStream fis;
		String message = "";
		try {
			fis = new FileInputStream(file);
			byte[] data = new byte[(int) file.length()];
			fis.read(data);
			fis.close();
			message = new String(data, "UTF-8");
		} catch (IOException e) {
			e.printStackTrace();
		}

		return message;
	}
	
}
