package com.serverless;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;

public class BayesTrainer {

	private Double totalSpam;
	private Double totalHam;
	private HashMap<String, Double> spammicity;
	private HashMap<String, Double> hammicity;

	public BayesTrainer() {
		
		this.totalSpam  = 0.0;
		this.totalHam   = 0.0;
		this.spammicity = new HashMap<String, Double>();
		this.hammicity  = new HashMap<String, Double>();
		
	}

	public void learnHam(String str) {
		WordTokenizer tokenizer = new WordTokenizer(str);
		HashSet<String> table   = tokenizer.createTable();
		Double total            = this.totalHam;
		String[] hamKeys        = this.hammicity.keySet().toArray(new String[this.hammicity.size()]);

		for (String word : hamKeys) {
			double oldHammicity = this.hammicity.get(word);

			if (!table.contains(word)) {
				this.hammicity.put(word, (total * oldHammicity)/(total + 1.0));
			}
			else {
				this.hammicity.put(word, (total * oldHammicity + 1.0)/(total + 1.0));
				table.remove(word);
			}

		}

		for (String word : table) {
			this.hammicity.put(word, 1.0/(total + 1.0));
		}

		this.totalHam = total + 1.0;
	}

	public void learnSpam(String str) {
		WordTokenizer tokenizer = new WordTokenizer(str);
		HashSet<String> table   = tokenizer.createTable();
		Double total            = this.totalSpam;
		String[] spamKeys       = this.spammicity.keySet().toArray(new String[this.spammicity.size()]);

		for (String word : spamKeys) {
			double oldSpammicity = this.spammicity.get(word);

			if (!table.contains(word)) {
				this.spammicity.put(word, (total * oldSpammicity)/(total + 1.0));
			}
			else {
				this.spammicity.put(word, (total * oldSpammicity + 1.0)/(total + 1.0));
				table.remove(word);
			}
		}

		for (String word : table) {
			this.spammicity.put(word, 1.0/(total + 1.0));
		}

		this.totalSpam = total + 1.0;
	}

	public void train(Boolean ham, String message) {
		if(ham) {
			this.learnHam(message);
		}else {
			this.learnSpam(message);
		}
	}

	public String printModel() {
		StringBuilder modelString = new StringBuilder();
		modelString.append("Total Spam::" + this.totalSpam + "\n");
		modelString.append("Total Ham::"  + this.totalHam + "\n");
		modelString.append("Spammicity::" + Arrays.toString(this.spammicity.entrySet().toArray()) + "\n");
		modelString.append("Hammicity::"  + Arrays.toString(this.hammicity.entrySet().toArray()) + "\n");
		return modelString.toString();
	}

	public void saveModel(String directoryName) {
		File directory = new File(directoryName);
		if(!directory.exists()){
			directory.mkdir();
		}
		this.saveObj(this.hammicity, directoryName + "/hammicity");
		this.saveObj(this.spammicity, directoryName + "/spammicity");
		this.saveObj(this.totalHam, directoryName + "/totalHam");
		this.saveObj(this.totalSpam, directoryName + "/totalSpam");
	}

	private void saveObj(Object obj, String fileName) {
		try{
			FileOutputStream fos = new FileOutputStream(fileName);
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(obj);
			oos.close();
			fos.close();
		}catch(IOException ioe) {
			ioe.printStackTrace();
		}
	}
}
